﻿namespace StaticLab
{
    public class StaticAppSetting
    {
        private static string _someSetting = "abc";
        public static string SomeSetting = _someSetting;

        public static void SetSetting(string settingValue)
        {
            _someSetting = settingValue;
        }

        public static string GetPrivateSomeSetting()
        {
            return _someSetting;
        }
    }
}