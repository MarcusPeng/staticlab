﻿using System.Collections.Generic;

namespace StaticLab
{
    public enum EnumAbc
    {
        A,
        B,
        C
    };

    public abstract class StaticBase
    {
        private static string _staticString;
        private static EnumAbc _staticEnum;
        private static Dictionary<int, int> _staticDictionary;
        private static int _staticInt;

        public void SetStaticString(string stringWantToSet)
        {
            _staticString = stringWantToSet;
        }

        public void SetStaticEnum(EnumAbc enumWantToSet)
        {
            _staticEnum = enumWantToSet;
        }

        public void SetStaticDictionary(Dictionary<int, int> dictionaryWantToSet)
        {
            _staticDictionary = dictionaryWantToSet;
        }

        public void SetStaticInt(int intWantToSet)
        {
            _staticInt = intWantToSet;
        }

        public string GetStaticString()
        {
            return _staticString;
        }

        public int GetStaticInt()
        {
            return _staticInt;
        }

        public Dictionary<int, int> GetStaticDictionary()
        {
            return _staticDictionary;
        }

        public EnumAbc GetStaticEnum()
        {
            return _staticEnum;
        }
    }
}