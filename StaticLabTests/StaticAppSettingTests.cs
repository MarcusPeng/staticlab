﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StaticLab;

namespace StaticLabTests
{
    [TestClass]
    public class StaticAppSettingTests
    {
        [TestMethod]
        public void SetSettingTest()
        {
            //Make sure SomeSetting initial set to abc
            Assert.AreEqual("abc", StaticAppSetting.SomeSetting);

            //Change setting to cba
            StaticAppSetting.SetSetting("cba");

            //Make sure setting value set to cba
            Assert.AreEqual("cba", StaticAppSetting.GetPrivateSomeSetting());

            //SomeSetting will not change, still abc
            Assert.AreEqual("abc", StaticAppSetting.SomeSetting);
        }
    }
}