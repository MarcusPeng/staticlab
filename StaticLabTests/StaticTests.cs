﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using StaticLab;

namespace StaticLabTests
{
    [TestClass]
    public class StaticTests
    {
        [TestMethod]
        public void StaticStringTest()
        {
            var subClassA = new SubClassA();
            var subClassB = new SubClassB();
            var aClassSet = "hello";
            var bClassSet = "world";

            subClassA.SetStaticString(aClassSet);
            subClassB.SetStaticString(bClassSet);

            Assert.AreEqual(bClassSet, subClassA.GetStaticString());
            Assert.AreEqual(bClassSet, subClassB.GetStaticString());
        }

        [TestMethod]
        public void StaticIntTest()
        {
            var subClassA = new SubClassA();
            var subClassB = new SubClassB();

            subClassA.SetStaticInt(1);
            subClassB.SetStaticInt(2);

            Assert.AreEqual(2, subClassA.GetStaticInt());
            Assert.AreEqual(2, subClassB.GetStaticInt());
        }

        [TestMethod]
        public void StaticEnumTest()
        {
            var subClassA = new SubClassA();
            var subClassB = new SubClassB();

            subClassA.SetStaticEnum(EnumAbc.A);
            subClassB.SetStaticEnum(EnumAbc.B);

            Assert.AreEqual(EnumAbc.B, subClassA.GetStaticEnum());
            Assert.AreEqual(EnumAbc.B, subClassB.GetStaticEnum());
        }

        [TestMethod]
        public void StaticDictionaryTest()
        {
            var subClassA = new SubClassA();
            var subClassB = new SubClassB();

            subClassA.SetStaticDictionary(new Dictionary<int, int> { { 1, 1 } });
            subClassB.SetStaticDictionary(new Dictionary<int, int> { { 2, 2 } });

            CollectionAssert.AreEqual(new Dictionary<int, int> { { 2, 2 } }, subClassA.GetStaticDictionary());
            CollectionAssert.AreEqual(new Dictionary<int, int> { { 2, 2 } }, subClassB.GetStaticDictionary());
        }
    }
}